import Image from "next/image";
import profilePic from "../../public/images/profile.jpg"
import styles from "./Profile.module.css"

const Profile =()=>{
  return(
    <div>
      <Image 
        src={profilePic}
        alt="pic of auth"
        placeholder="blur"
        width={400}
        height={400}
        className={styles.radius}
      />
    </div>
  )
}
export default Profile;
